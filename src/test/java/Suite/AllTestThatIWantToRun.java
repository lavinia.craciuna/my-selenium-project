package Suite;

import dataDriven.DataDrivenTests;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({MediumTest.class,
BasicTests.class,
        DataDrivenTests.class})
public class AllTestThatIWantToRun {}

