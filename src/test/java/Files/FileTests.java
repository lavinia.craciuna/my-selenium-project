package Files;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class FileTests {
    /**
     * Just extract the file name from a mythical path
     */
    @Test
    public void testFileNameExtractionFromPath(){
        File winFile=new File ("C:\\beta\\gama.txt");
        Assert.assertEquals("gama.txt",winFile.getName());

        File unixFile=new File ("C:/beta/gama.txt");
        Assert.assertEquals("gama.txt",unixFile.getName());
        Assert.assertFalse(unixFile.exists());
    }
}
