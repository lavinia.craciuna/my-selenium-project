package database;
import org.junit.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DbExampleTest {
    private Connection connection;
    private Statement statement;
    @Before
    public void setup() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://88.198.169.215:3306/wordpress_wantsome", "wantsomeUser", "apxO9$53");
            statement = connection.createStatement();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    @Test
    public void test_1() {
        try {

            ResultSet resultSet = statement.executeQuery("SELECT * FROM wantsome");
            while (resultSet.next()){
                System.out.println("Row with number " + resultSet.getRow() + " has ID " + resultSet.getInt("id"));
            }

            resultSet.close();

        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    @Test
    public void test_2() {
        try {

            ResultSet resultSet = statement.executeQuery("SELECT * FROM wantsome");
            List<String> names=new ArrayList<>();
            while (resultSet.next()){
                String name=resultSet.getString("name");
                names.add(name);
            }
            System.out.println(names);
            resultSet.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_3() {
        try {
            PreparedStatement preparedStatement=connection.prepareStatement("SELECT * FROM wantsome WHERE name LIKE?");
            preparedStatement.setString(1,"b%");
            ResultSet resultSet = preparedStatement.executeQuery();
            List<String> names=new ArrayList<>();
            while (resultSet.next()){
                String name=resultSet.getString("name");
                names.add(name);
            }
            System.out.println(names);
            preparedStatement.close();
            resultSet.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @After
    public void cleanup() {
        try {
            connection.close();
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}
