package dataDriven;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@RunWith(DataProviderRunner.class)
public class DataDrivenTests {

    @DataProvider
    public static Object[][] powers() throws IOException {
        List<Object[]> outputList=new ArrayList<>();
        List<String> records=readAllLinesFromResourcesFile("user_db.csv");
        for (String record:records) {
            String[] params=record.split(",");
            outputList.add(params);

        }
        Object[][] result=new Object[records.size()][];
        outputList.toArray(result);
        return result;
    }

    @UseDataProvider("powers")
    @Test
    public void test(String user, String email,String password) {
        System.out.println("user = " + user + ", email = " + email + ", password = " + password);
    }
    /**
     * Can read a given file from the resources folder
     * @param fileName file name
     * @return list of all the lines in the file
     */

    public static List<String> readAllLinesFromResourcesFile(String fileName) throws IOException {
        List<String> result = new ArrayList<>();
        ClassLoader classLoader = DataDrivenTests.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {

            String line;
            while ((line = br.readLine()) != null) {
                result.add(line);
            }
        }
        return result;
    }

}
