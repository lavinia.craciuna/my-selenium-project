package dataDriven;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

public class PropertiesTests {
    @Test(timeout = 6000)
    public void loadPropertiesTest() throws IOException {
        File file= getFileFromResources("dev_props.properties");
        FileInputStream propFile= new FileInputStream(file);

        Properties properties = new Properties();
        properties.load(propFile);
        String browser=properties.getProperty("browser");
        Assert.assertEquals("chrome",browser);
        String addr=properties.getProperty("server_addr");
        Assert.assertEquals("localhost",addr);
        String port=properties.getProperty("port");
        port=port==null ?"80":port;
        Assert.assertEquals("80",port);

    }
        public static File getFileFromResources (String fileName){
            ClassLoader classLoader = DataDrivenTests.class.getClassLoader();
            URL resource = classLoader.getResource(fileName);
            File file = null;
            if (resource == null) {
                throw new IllegalArgumentException("file not found!");
            } else {
                file = new File(resource.getFile());
            }
            return file;
        }
    }

