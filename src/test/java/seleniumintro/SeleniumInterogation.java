package seleniumintro;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Arrays;

import static seleniumintro.DriversUtils.setWebDriverProperty;

public class SeleniumInterogation {
    private static WebDriver driver;

    @BeforeClass
    public static void setup() {
        setWebDriverProperty("chrome");
        driver = new ChromeDriver();
    }
    @Test
    public void testTitle(){
        driver.get("https://practica.wantsome.ro/blog/");
        String title=driver.getTitle();
        System.out.println("now title is: "+title);
        String[] words=title.split(" ");
        System.out.println("after spliting array is: "+ Arrays.toString(words));
        int numberOfWordsWithMoreThanFiveLetters=0;
        for(String word: words)
            if(word.length()>5){
                numberOfWordsWithMoreThanFiveLetters++; // daca este mai mare decat 5 litere sa mearga la urmatorul cuvant
            }
        Assert.assertEquals(5,numberOfWordsWithMoreThanFiveLetters);
    }

    @Test
    public void testTextPresentInSourcePage(){
        driver.get("https://practica.wantsome.ro/blog/");
        Assert.assertTrue(driver.getPageSource().contains("The roof is on fireeee"));
    }


    @Test
    public void pageSourceTest(){
        driver.get("\"https://practica.wantsome.ro/blog/\"");
        //System.out.println(driver.getPageSource());
        System.out.println("Current uls is: "+driver.getCurrentUrl());
        System.out.println("Current title is: "+driver.getTitle());
    }


   @AfterClass
    public static void teardown() {
        driver.close();//tear down
    }
}
