package seleniumintro;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ro.shop.ShopLoginPage;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class PageObjectLoginTests extends BaseTestClass {

    @Before
    public void before(){driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);}

    @Test
    public void testValidLogin(){
        String username="test5@test.com";
        String password="Testtest@0123";
        navigateToLoginPage();
        ShopLoginPage shopLoginPage=new ShopLoginPage(driver);
        shopLoginPage.fillUsernameFiled(username);
        shopLoginPage.fillPasswordFiled(password);
        shopLoginPage.clickLoginButton();
        WebElement myAcountTitle=driver.findElement(By.cssSelector(".entry-title"));
        assertEquals("MY ACOUNT", myAcountTitle.getText());

    }
    private void navigateToLoginPage() {
        driver.findElement(By.xpath("//div[contains(@class, 'user-wrapper')]/a[@class = 'user-icon']")).click();
    }
}
