package seleniumintro;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.net.URL;

import static seleniumintro.DriversUtils.setWebDriverProperty;

public class IntroductionExercises {

    /**
     * Start a new Chrome instance. Navigate to http://practica.wantsome.ro/blog/
     */
    private WebDriver driver;
    private String baseUrl = "http://practica.wantsome.ro/blog/";
    private DriversUtils driversPath = new DriversUtils();

    @Before
    public void setupTest() {
        System.setProperty("webdriver.chrome.driver", driversPath.getDriversPath() + "chromedriver" + driversPath.getFileExtension());
    }


    /**
     * and perform the following operations:
     * - print the page URL
     */
    @Test
    public void firstBlogTest() {
        driver = new ChromeDriver();
        driver.get(baseUrl);
        System.out.println("url-ul paginii este: " + driver.getCurrentUrl());
        /** - print the page title
         */
        System.out.println("titlul paginii este: " + driver.getTitle());
        /** - assert the fact that the page source contains 'Wantsome Iasi'
         */
        Assert.assertTrue(driver.getPageSource().contains("Wantsome Iasi"));
        driver.close();
    }


    /**
     * Start a new Chrome instance. Navigate to http://practica.wantsome.ro/blog/
     * and perform the following actions:
     * - navigate to contact page and check page title
     */

    @Test
    public void blogInterrogationTest() {
        driver = new ChromeDriver();
        driver.get(baseUrl);
        driver.findElement(By.linkText("Contact")).click();
        Assert.assertEquals("Contact – Wantsome Iasi", driver.getTitle());

        /** - navigate to login page and check that page source contains 'Remember Me'*/
        driver.navigate().to("https://practica.wantsome.ro/blog/login/");
        Assert.assertTrue(driver.getPageSource().contains("Remember Me"));

        /** - navigate to register page and check the current URL
         */
        driver.findElement(By.linkText("Register")).click();
        Assert.assertEquals("https://practica.wantsome.ro/blog/register/", driver.getCurrentUrl());
        driver.close();
    }

    /**
     * Start a new Chrome instance. Navigate to http://practica.wantsome.ro/blog/
     * and perform the following operations:
     * - close the browser
     */
    @Test
    public void navigationTest() {
        driver = new ChromeDriver();
        driver.get(baseUrl);
        driver.close();

        /** - start again the browser and open the same URL
         */
        driver = new ChromeDriver();
        driver.get(baseUrl);

         /** - navigate to Login page and check that you are on Login page (you have to think about a solution for this)
          */
        driver.navigate().to("https://practica.wantsome.ro/blog/login/");
        Assert.assertTrue(driver.getPageSource().contains("Log In"));

         /** - navigate back and check you are again on homepage*/
         driver.navigate().back();
         Assert.assertEquals("Wantsome Iasi – Practice website for testing sessions",driver.getTitle());

         /** - navigate to Register page*/
        driver.findElement(By.linkText("Register")).click();
         /** - go back*/
        driver.navigate().back();

         /** - navigate forward and check you are on Register page*/
        driver.navigate().forward();
        Assert.assertEquals("https://practica.wantsome.ro/blog/register/", driver.getCurrentUrl());

         /** - go back*/
        driver.navigate().back();

         /** - refresh the page and check you are on homepage*/
        driver.navigate().refresh();
        Assert.assertEquals("Wantsome Iasi – Practice website for testing sessions",driver.getTitle());
         /** - close the browser
         */
         driver.close();
    }
}

