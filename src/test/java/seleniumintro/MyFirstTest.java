package seleniumintro;

import org.apache.commons.lang3.SystemUtils;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import static seleniumintro.DriversUtils.setWebDriverProperty;

public class MyFirstTest {
    private  WebDriver driver;
    @Before
    public void setup() {
        setWebDriverProperty("chrome");
        driver = new ChromeDriver();
    }

    @Test
    public void openChromeBrowser1() throws InterruptedException {
        //System.setProperty("webdriver.chrome.driver","C:\\code\\my-selenium-project\\src\\test\\resources\\drivers\\windows\\chromedriver.exe");
        setup();
        setWebDriverProperty("chrome");


        driver.get("https://practica.wantsome.ro/blog/");
        String pageTitle = driver.getTitle();// returneaza titlul paginii curente
        System.out.println(pageTitle);

        driver.navigate().to("https://www.emag.ro/");
        Assert.assertNotEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        Assert.assertEquals("eMAG.ro - Libertate în fiecare zi", driver.getTitle());

        driver.navigate().back();
        Assert.assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());

        driver.navigate().forward();
        Assert.assertEquals("eMAG.ro - Libertate în fiecare zi", driver.getTitle());

        teardown();
    }

    @Test
    public void openChromeBrowser2() throws InterruptedException {
        //System.setProperty("webdriver.chrome.driver","C:\\code\\my-selenium-project\\src\\test\\resources\\drivers\\windows\\chromedriver.exe");
        setup();
        setWebDriverProperty("chrome");

        driver.get("https://practica.wantsome.ro/blog/");
        String pageTitle = driver.getTitle();// returneaza titlul paginii curente
        System.out.println(pageTitle);

        driver.navigate().to("https://www.emag.ro/");
        Assert.assertNotEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        Assert.assertEquals("eMAG.ro - Libertate în fiecare zi", driver.getTitle());
        teardown();
    }


    @Test
    public void openHtmlUnitBrowser() throws InterruptedException {
        // nu are interfata grafica, deci nu se deschide efectiv o pagina in browser
        // nu este nevoie sa setam un System property


        driver.get("https://practica.wantsome.ro/blog/");
        String pageTitle = driver.getTitle();// returneaza titlul paginii curente
        System.out.println(pageTitle);
        Thread.sleep(5000);// cate milisecunde dorim sa tinem pagina deschisa
        driver.close();//tear down
        teardown();
    }
    @After

    public void teardown() {
        driver.close();//tear down
    }

}
