package seleniumintro;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class ManipulationTests {
    private WebDriver driver;
    private String baseUrl = "http://practica.wantsome.ro/blog";
    private DriversUtils driversPath = new DriversUtils();

    @Before
    public void setupTest() {
        System.setProperty("webdriver.chrome.driver", driversPath.getDriversPath() + "chromedriver" + driversPath.getFileExtension());
        driver = new ChromeDriver();
        driver.get(baseUrl);
    }

    @Test
    public void selectTest() {
        driver.navigate().to("http://practica.wantsome.ro/shop/?product_cat=men-collection");
        WebElement sortDropDown = driver.findElement(By.cssSelector("select.orderby"));
        Select sortSelect = new Select(sortDropDown);
        sortSelect.selectByIndex(2);
        //Assert.assertTrue(true);
        Assert.assertTrue(driver.getCurrentUrl().contains("orderby=rating"));
    }

    @Test
    public void verifyCheckoutCartButtonHover() throws InterruptedException {
        driver.navigate().to("http://practica.wantsome.ro/shop/");
        By specialXPath = getProductCartButton("Men’s watch");
        //WebElement addToCartButton= driver.findElement(By.cssSelector(".add_to_cart_button.ajax_add_to_cart"));
        WebElement addToCartButton = driver.findElement(specialXPath);
        scrollToElement(addToCartButton);
        addToCartButton.click();
        Thread.sleep(500);

        WebElement cardTopElement = driver.findElement(By.xpath("//a[@class='wcmenucart-contents']"));
        scrollToElement(cardTopElement);
        new Actions(driver)
                .moveToElement(driver.findElement(By.xpath("//a[@class='wcmenucart-contents']")))
                .build()
                .perform();

        //Action mouseOver=builder.moveToElement(cardTopElement).build();
        //mouseOver.perform();

        String xpathLocatorViewCart = "//div[@class='widget woocommerce widget_shopping_cart']//a[contains(text(),'View cart')]";
        WebElement viewCartElement = driver.findElement(By.xpath(xpathLocatorViewCart));
        Assert.assertTrue(viewCartElement.isDisplayed());
        viewCartElement.click();
    }

    private void scrollToElement(WebElement addToCartButton) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", addToCartButton);
    }

    public By getProductCartButton(String product) {
        return By.xpath("//a[contains(text(),'" + product + "')]/parent::*/parent::*/div/a");
    }
}

