package seleniumintro;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class InterrogationExercises {
    private WebDriver driver;
    private String baseUrl = "http://practica.wantsome.ro/blog/";
    private DriversUtils driversPath = new DriversUtils();

    @Before
    public void setupTest() {
        System.setProperty("webdriver.chrome.driver", driversPath.getDriversPath() + "chromedriver" + driversPath.getFileExtension());
        driver = new ChromeDriver();
        driver.get(baseUrl);
    }
    /**
     * 1. Go to http://practica.wantsome.ro/shop
     * 2. Identify the Blog top menu item
     * 3. Print the CSS color property value for Blog menu item
     * 4. Print the value of the href property
     * 5. Print the tag name of the Blog menu item
     * 6. Print the text of the Blog menu item
     */
    @Test
    public void exercise1() {
        WebElement mainTitle=driver.findElement(By.xpath("//a[contains(text(),'Wantsome Iasi')]"));
        Assert.assertEquals("Wantsome Iasi",mainTitle.getText());
        Assert.assertTrue(mainTitle.getAttribute("href").contains("wantsome.ro"));
        Assert.assertTrue(mainTitle.isDisplayed());
        Assert.assertTrue(mainTitle.isEnabled());
        Assert.assertEquals("a",mainTitle.getTagName());


    }

    /**
     * 1. Go to http://practica.wantsome.ro/shop
     * 2. Access Connect page
     * 3. Find different approaches (id, class name, tag name, css selector or any other)
     *      in order to identify the text-box for Login username (left side)
     */
    @Test
    public void exercise2() {

    }

    /**
     * 1. Go to http://practica.wantsome.ro/shop
     * 2. Access Connect page
     * 3. Make use of ByChained support class in order to identify the password
     *      field for the registration form (right side)
     * 4. Make use of ByChained support class in order to identify the Login button
     */
    @Test
    public void exercise3() {

    }

    /**
     * 1. Go to http://practica.wantsome.ro/shop
     * 2. Find locator that identifies ALL primary-menu items (home, blog, cart, checkout, connect)
     * 3. Make use of findElements() method
     * 4. Check that primary-menu contains 5 menu items
     */
    @Test
    public void exercise4() {

    }
}

