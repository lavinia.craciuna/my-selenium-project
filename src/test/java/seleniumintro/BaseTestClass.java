package seleniumintro;

import blog.utils.BrowserFactory;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTestClass {
    public WebDriver driver;
    private String baseUrl = "http://testare-automata.practica.tech/shop";
    private DriversUtils driversPath = new DriversUtils();

    @Before
    public void setupTest() {
        //System.setProperty("webdriver.chrome.driver", driversPath.getDriversPath() + "chromedriver" + driversPath.getFileExtension());
        driver= BrowserFactory.getDriver("chrome");
        //driver = new ChromeDriver();
        driver.get(baseUrl);
    }

    @After
    public void tearDown(){driver.quit();}

}
