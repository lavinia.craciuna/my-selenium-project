package seleniumintro;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class LocatorTest {
    private WebDriver driver;
    private String baseUrl = "http://practica.wantsome.ro/blog/";
    private DriversUtils driversPath = new DriversUtils();
    protected WebDriverWait wait;

    @Before
    public void setupTest() {
        System.setProperty("webdriver.chrome.driver", driversPath.getDriversPath() + "chromedriver" + driversPath.getFileExtension());
        driver = new ChromeDriver();
        driver.get(baseUrl);
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void findByCSS(){

        WebElement searchBox=driver.findElement(By.cssSelector(".search-field")); //clasele se identifica cu ., titlurile cu #
        Assert.assertEquals(searchBox.getAttribute("title"),"Search for:");
        Assert.assertEquals("",searchBox.getText());//
        searchBox.sendKeys("Alphabet");//cautam textul Alphabet
        Assert.assertEquals("Alphabet",searchBox.getAttribute("value"));//verificam ca textul din search box este Alphabet
    }
    @Test
    public void findTitleBySelector(){

        List<WebElement> postTitle=driver.findElements(By.cssSelector("header>h2.enttry-title"));
       for(WebElement element: postTitle)
           Assert.assertTrue(element.getText().contains("Test"));
    }
    @Test
    public void findByXPath(){
        WebElement searchBox =driver.findElement(By.xpath("//div[@id='logo']"));
        Assert.assertNotNull(searchBox);
    }
    @Test
    public void findByXPathTest(){
        WebElement searchBox=driver.findElement(By.cssSelector(".search-field"));
        WebElement searchButton=driver.findElement(By.className("search-submit"));

        searchBox.sendKeys("lorem");
        searchButton.click();
        WebElement searchResultsLabel=driver.findElement(By.xpath("//h1[@class='archive-title']"));
        Assert.assertTrue(searchResultsLabel.getText().contains("Search Results"));

    }
    @Test
    public void cookiesAreAwesome(){
        driver.navigate().to("http://practica.wantsome.ro/shop/");
        Set<Cookie> someCookies=driver.manage().getCookies();
        driver.manage().addCookie(new Cookie("Wantsome cookies?","Yes, please"));
        Assert.assertEquals(2,driver.manage().getCookies().size());
        //driver.manage().deleteAllCookies();
    }
    @Test
    public void testSlowLoadingElement(){
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(40,TimeUnit.SECONDS);// astept sa se incarce pagina, o folosim cand avem multe teste
        driver.navigate().to("https://singdeutsch.com");
        By titlePath = By.xpath("//h1[@class='site-title h1']");
        wait.until(ExpectedConditions.elementToBeClickable(titlePath));
        Assert.assertEquals("Sing Deutsch".toUpperCase(),driver.findElement(titlePath).getText());

    }
}
