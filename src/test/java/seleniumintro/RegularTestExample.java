package seleniumintro;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class RegularTestExample extends BaseTestClass {

    String url = "http://testare-automata.practica.tech/shop";

    @Before
    public void before() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }


    // As a User I can click on My Account so that I reach the Login form
    // - navigation
    // - login page elements (initial status - e.g. fields are empty, elements are displayed/not displayed, etc.)
    //
    @Test
    public void loginPageNavigation() {
        driver.navigate().to(url);
        navigateToLoginPage();
        assertEquals("MY ACCOUNT", driver.findElement(By.cssSelector(".page-header .tg-container h2")).getText());
    }

    private void navigateToLoginPage() {
        driver.findElement(By.xpath("//div[contains(@class, 'user-wrapper')]/a[@class = 'user-icon']")).click();
    }

    @Test
    public void elementPresent(){
        driver.navigate().to(url);
        navigateToLoginPage();
        WebElement userNameFiled=driver.findElement(By.id("username"));
        Assert.assertTrue(userNameFiled.isDisplayed());
        WebElement passwordFiled=driver.findElement(By.id("password"));
        Assert.assertTrue(passwordFiled.isDisplayed());
        WebElement loginButton=driver.findElement(By.xpath("//input[@name='login']"));
        Assert.assertTrue(loginButton.isDisplayed());

    }
    @Test
    public void testValidTest(){
        driver.navigate().to(url);
        navigateToLoginPage();
        String username="test5@test.com";
        String password="Testtest@0123";
        WebElement userNameFiled=driver.findElement(By.id("username"));
        WebElement passwordFiled=driver.findElement(By.id("password"));
        WebElement loginButton=driver.findElement(By.xpath("//input[@name='login']"));

        userNameFiled.sendKeys(username);
        passwordFiled.sendKeys(password);
        loginButton.click();

        WebElement myAcountTitle=driver.findElement(By.cssSelector(".entry-title"));
        assertEquals("MY ACOUNT", myAcountTitle.getText());
    }

    // As a User I can enter my username and password in the Login form so that I can login into the application
    // - successful case (valid username + password)
    // - successful case (valid email + password)
    // - other cases (let's be creative :) )
}
