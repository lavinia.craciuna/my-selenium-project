package seleniumintro;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import ro.shop.User;

public class ShopLoginPageFactoryTest extends BaseTestClass {
    @Test
    public void testLoginShopUsingFactory(){
        driver.get("https://testare-automata.practica.tech/shop/?page_id=11");
        ShopLoginPageFactory loginPageFactory = PageFactory.initElements(driver,ShopLoginPageFactory.class);
        loginPageFactory.fillUsername("test5@test.com");
        loginPageFactory.fillPassword("Testtest@0123");
        loginPageFactory.clickLogin();

        WebElement myAccountTitle=driver.findElement(By.cssSelector(".entry-title"));
        Assert.assertEquals("MY ACCOUNT", myAccountTitle.getText());
    }

    @Test
    public void testLogin_1(){
        driver.get("https://testare-automata.practica.tech/shop/?page_id=11");
        ShopLoginPageFactory loginPage=PageFactory.initElements(driver,ShopLoginPageFactory.class);
        User user =new User.Builder().withUsername("test5@test.com").withPassword("Testtest@0123").build();
        loginPage.loginWithEmailUser(user);
        WebElement myAccountTitle=driver.findElement(By.cssSelector(".entry-title"));
        Assert.assertEquals("MY ACCOUNT", myAccountTitle.getText());
    }
}
