package seleniumintro;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ro.shop.HomePageCollection;
import ro.shop.ShopHomePageContent;
import ro.shop.ShopHomePagePRoductList;
import ro.shop.ShopHomePageProduct;

import java.util.concurrent.TimeUnit;

public class HomePageTestDemo extends BaseTestClass {
    @Before
    public void before() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }
    @Test
    public void checkMenCollectionTitle(){
        String expectedTitle="Men Collection";

        ShopHomePageContent homePageContent=new ShopHomePageContent(driver);
        HomePageCollection homePageCollection=homePageContent.getShopHomePageCollectionByIndex(1);
        String actualTitle=homePageCollection.getCollectionTitle();
        Assert.assertEquals(expectedTitle,actualTitle);

    }

    @Test
    public void checkWomenCollectionTitle(){
        String expectedTitle="Women Collection";

        ShopHomePageContent homePageContent=new ShopHomePageContent(driver);
        HomePageCollection homePageCollection=homePageContent.getShopHomePageCollectionByIndex(2);
        String actualTitle=homePageCollection.getCollectionTitle();
        Assert.assertEquals(expectedTitle,actualTitle);

    }
    @Test
    public void checkNumberOfProductsInFirstListMenCollection(){
        ShopHomePageContent homePageContent=new ShopHomePageContent(driver);
        HomePageCollection homePageCollection=homePageContent.getShopHomePageCollectionByIndex(1);
        ShopHomePagePRoductList homePagePRoductList=homePageCollection.getProductList(1);
        Assert.assertEquals(5,homePagePRoductList.getNumberOfPRoducts());

    }

    @Test
    public void addToCardProductNo1(){
        ShopHomePageContent homePageContent=new ShopHomePageContent(driver);
        HomePageCollection homePageCollection=homePageContent.getShopHomePageCollectionByIndex(1);
        ShopHomePagePRoductList productList=homePageCollection.getProductList(1);
        ShopHomePageProduct classic_watch_product=productList.getProductListElement("Classic Watch");
        Assert.assertEquals("Classic Watch",classic_watch_product.getTitle());
        Assert.assertEquals("Price: 70,00 lei",classic_watch_product.getPrice());
    }
}
