package seleniumintro;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SyncrronizationExercises {
    private WebDriver driver;
    private String baseUrl = "http://whitelotus.cristiancotoi.ro";
    private DriversUtils driversPath = new DriversUtils();

    @Before
    public void setupTest() {
        System.setProperty("webdriver.chrome.driver", driversPath.getDriversPath() + "chromedriver" + driversPath.getFileExtension());
        driver = new ChromeDriver();
        driver.get(baseUrl);
    }

    @Test
    public void localStorageTest() {
        ((JavascriptExecutor) driver).executeScript("window.localStorage.setItem('lavinia','lavinia.craciuna@gmail.com')");
        driver.navigate().to("http://whitelotus.cristiancotoi.ro/#/calendar");
    }

}
