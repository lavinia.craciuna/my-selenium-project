package blog.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import seleniumintro.DriversUtils;

public class BrowserFactory {
    public static WebDriver getDriver(String browserName) {
        DriversUtils.setWebDriverProperty(browserName);
        switch (browserName) {
            case "chrome":
                return new ChromeDriver();
            case "firefox":
                return new FirefoxDriver();
            case "ie":
                return new InternetExplorerDriver();

        }
        throw new RuntimeException("this browser is not supported: "+ browserName);
    }
}

