package api;

import io.restassured.http.ContentType;
import org.junit.Assert;
import org.junit.Test;
import ro.apiModels.Pet;

import java.util.HashMap;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class TestObjects {
    @Test
    public void testCreatePetUsingSerialisation(){
        Pet pet=new Pet("Wantsome");
        given()
                .contentType(ContentType.JSON)
                .body(pet)
                .when()
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .statusCode(200)
                .body("name",equalTo(pet.getName()));
    }
    @Test
    public void testCreatePetUsingMap(){
        HashMap<String,Object> objectHashMap=new HashMap<>();
        objectHashMap.put("name","MapPET");
        objectHashMap.put("status","alive");
        given()
                .contentType(ContentType.JSON)
                .body(objectHashMap)
                .when()
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .statusCode(200)
                .body("name",equalTo(objectHashMap.get("name")));
        System.out.println(objectHashMap);

    }
    @Test
    public void testCreatePetDeserialization(){
     Pet retrievePet=given().pathParam("petId",1000036)
             .get("https://petstore.swagger.io/v2/pet/{petId}").as(Pet.class);
        Assert.assertThat(retrievePet.getName(),is(equalTo("Pet 1")));

    }

}




