package api;
import io.restassured.http.ContentType;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.codehaus.groovy.tools.StringHelper;
import org.junit.Assert;
import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import org.junit.Test;

public class AuthorizationTest {
    @Test
    public void testAuthorization(){
        given().when().get("http://api.trello.com/1/boards/IZljKxvY/")
                .then().statusCode(HttpStatus.SC_UNAUTHORIZED);
    }
    @Test
    public void testAuthorization_2(){
        given().auth().oauth("239e57e0eb4eb89090f1d2ee87e55a30","564cdbae1fc9ab19f191b522f57cad29b605072195c27b744cc85883ddc9e935","0c637911f345b140343d88f85166683b85a38d5c6e2cdfef97a097611f6ba916","")
                .when().get("http://api.trello.com/1/boards/IZljKxvY/")
                .then().statusCode(200).extract().response().prettyPrint();
    }
    @Test
    public void testAuthorization_3(){
        Response response=given().auth().oauth("239e57e0eb4eb89090f1d2ee87e55a30","564cdbae1fc9ab19f191b522f57cad29b605072195c27b744cc85883ddc9e935","0c637911f345b140343d88f85166683b85a38d5c6e2cdfef97a097611f6ba916","")
                .when().get("http://api.trello.com/1/boards/IZljKxvY/")
                .then()
                .body("name", equalTo("Wantsome"))
                .and().body("url",is("https://trello.com/b/IZljKxvY/wantsome"))
                .statusCode(200).extract().response();
        String json=response.asString();
        JsonPath jsonPath=new JsonPath(json);
        String boardName=jsonPath.getString("name");
        Assert.assertEquals("Wantsome",boardName);
    }


}
