package api;

import io.restassured.http.ContentType;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;


public class JsonParsing {
    @BeforeClass
    public static void setUp(){
        authentication=basic("user","pass");
    }
    @Test
    public void testBodyPathSndFilter(){
        get("http://petstore.swagger.io/v2/pet/12345").then().
                body("tags.findAll { it.id > 100 }.name",
                        hasItems("wantsomePetTag1"));

    }
    @Test
    public void testGivenWhenThen(){
        given().contentType(ContentType.JSON)
        .when().get("http://petstore.swagger.io/v2/pet/12345")
                .then().body("id", is(12345))
                .body("category.name", is("wantsomePetCategory"))
                .body("status", is("available"))
                .statusCode(200);
    }
    @Test
    public void testResponse(){
        Response response = get("http://petstore.swagger.io/v2/pet/12345");
        response.prettyPrint();
    }
    @Test
    public void restJsonPath(){
        Response response=get("http://petstore.swagger.io/v2/pet/12345");
        String json=response.asString();
        System.out.println("The json string is: \n"+json);
        JsonPath jsonPath=new JsonPath(json);
        String actualCategory=jsonPath.getString("category.name");
        Assert.assertEquals("Wantsome Pet",actualCategory);
    }
    @Test
    public void testXmlPath(){
        Response response=given().header("Accept","application/xml")
                .get("http://petstore.swagger.io/v2/pet/12345");
        String xml=response.asString();
       // System.out.println();
        XmlPath xmlPath=new XmlPath(xml);
        xmlPath.setRootPath("Pet");
        String actualCategory=xmlPath.getString("category.name");
        System.out.println("Actual category="+actualCategory);
        Assert.assertEquals("Wantsome Pet",actualCategory);
    }
    @Test
    public void testTagJsonPath(){
        Response response=get("http://petstore.swagger.io/v2/pet/12345");
        String json=response.asString();
        JsonPath jsonPath=new JsonPath(json);
        String secondTagName=jsonPath.getString("tags[1].name");
        System.out.println(secondTagName);
        Assert.assertEquals("good doggy",secondTagName);
    }
    @Test
    public void testTagXmlPath(){
        Response response=given().header("Accept","application/xml")
                .get("http://petstore.swagger.io/v2/pet/12345");
        String xml=response.asString();
        XmlPath xmlPath=new XmlPath(xml);
        xmlPath.setRootPath("Pet");
        String secondTagName=xmlPath.getString("tags.tag[1].name");
        System.out.println(secondTagName);
        Assert.assertEquals("good doggy",secondTagName);
    }
    @Test
    public void testResponseHeader(){
        Response response=get("http://petstore.swagger.io/v2/pet/12345");
        Headers headers=response.getHeaders();
        Assert.assertEquals(7,headers.size());
        Assert.assertTrue(headers.hasHeaderWithName("Server"));

    }
    @Test
    public void testResponseHeaderDate(){
        Response response=get("http://petstore.swagger.io/v2/pet/12345");
        String date=response.getHeader("Date");
        System.out.println(date);
        String[] tokens=date.split(",");
        Assert.assertEquals("Sat",tokens[0]);

    }
    @Test
    public void testResponseStatusCode(){
        Response response=get("http://petstore.swagger.io/v2/pet/12345");
        int statusCode=response.statusCode();
        Assert.assertEquals(200,statusCode);
    }
    @Test
    public void firstTestPathParams_1(){
        given().when().get("http://petstore.swagger.io/v2/pet/{petId}",12345)
                .then().statusCode(200);
    }

    @Test
    public void firstTestPathParams_2(){
        given().when().get("http://petstore.swagger.io/{apiVersion}/pet/{petId}","v2",12345)
                .then().statusCode(200);
    }
    @Test
    public void firstTestPathParams_3(){
        given()
                .pathParam("apiVersion","v2")
                .pathParam("petId",12345)
                .when().get("http://petstore.swagger.io/{apiVersion}/pet/{petId}")
                .then()
                        .statusCode(200);
    }
    @Test
    public void testRetrievePet(){
        Response response=get("http://petstore.swagger.io/v2/pet/1234");
        int statusCode=response.statusCode();
        Assert.assertEquals(200,statusCode);
    }
    @Test
    public void testResponsTime(){
      Long responseTime=  given()
                .pathParam("petId",12345)
                .when().get("http://petstore.swagger.io/v2/pet/{petId}")
                .time();
        System.out.println(responseTime);
        Assert.assertThat(responseTime,is(lessThan(2000L)));
    }
    @Test
    public void testResponseTime_2(){
        Long responseTime=  given()
                .pathParam("petId",12345)
                .when().get("http://petstore.swagger.io/v2/pet/{petId}")
                .timeIn(TimeUnit.SECONDS);
        System.out.println(responseTime);
        Assert.assertThat(responseTime,is(lessThan(3L)));
    }
    @Test
    public void testResponseTime_3(){
        given()
                .pathParam("petId",12345)
                .get("http://petstore.swagger.io/v2/pet/{petId}")
                .then()
                .time(lessThan(3L),TimeUnit.SECONDS);
    }
    @Test
    public void testResponseTime_4(){
        Response response=given()
                .pathParam("petId",12345)
                .get("http://petstore.swagger.io/v2/pet/{petId}")
                .then()
                .time(lessThan(3L),TimeUnit.SECONDS).extract().response();
    }
}
