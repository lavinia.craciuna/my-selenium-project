package ro.shop;

import javafx.util.Builder;

public class User {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public User (Builder builder){
        this.username=builder.username;
        this.password=builder.password;
    }
    public static class Builder{
        private String username;
        private String password;

        public Builder withUsername(String username){
            this.username=username;
            return this;
        }

        public Builder withPassword(String password){
            this.password=password;
            return this;
        }
        public User build(){
            return new User(this);
        }
    }
}
