package ro.shop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShopHomePageContent {
    private WebDriver driver;
    @FindBy(xpath = "//h3[@class='slider-title']//a[contains(text(),'Men Collection')]")
    private WebElement menCollectionButton;
    @FindBy(xpath = "//h3[@class='slider-title']//a[contains(text(),'Women Collection')]")
    private WebElement womenCollectionButton;

    private static final String HOME_PAGE_COLLECTION_XPATH = "//section[@id='estore_woocommerce_product_grid-<<collectionNumber>>']";

    public ShopHomePageContent(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void clickOnMenCollection() {
        menCollectionButton.click();
    }

    public ShopHomePageContent() {
        womenCollectionButton.click();
    }
    public HomePageCollection getShopHomePageCollectionByIndex(int index){
       WebElement homePAgeCollectionContainer= driver.findElement(By.xpath(HOME_PAGE_COLLECTION_XPATH.replace("<<collectionNumber>>",String.valueOf(index))));
       return new HomePageCollection(homePAgeCollectionContainer);

    }
}
