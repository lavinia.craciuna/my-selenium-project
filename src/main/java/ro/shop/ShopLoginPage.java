package ro.shop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ShopLoginPage {
    private static final By LOGIN_TITLE=By.xpath("//h2[contains(text(),'Login')]");
    private static final By USERNAME_FIELD=By.id("username");
    private static final By PASSWORD_FIELD=By.id("password");
    private static final By LOGIN_BUTTON=By.xpath("//input[@name='login']");
    private static final By REMEMEBER_ME_CKECK=By.id("rememberme");

    private WebDriver driver;

    public ShopLoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public void fillUsernameFiled(String username) {
        WebElement usernameElement=driver.findElement(USERNAME_FIELD);
        usernameElement.clear();
        usernameElement.sendKeys(username);
    }

    public void fillPasswordFiled(String password) {
        driver.findElement(PASSWORD_FIELD).sendKeys(password);
    }

    public  void clickLoginButton() {
        driver.findElement(LOGIN_BUTTON).click();

    }

}
