package ro.shop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ShopHomePagePRoductList {
    private WebElement container;
    private static final String PRODUCT_XPATH = "./div";
    private static final String PRODUCT_WITH_NAME = PRODUCT_XPATH + "[//a[contains([text()='<<product-name>>)]']";

    public ShopHomePagePRoductList(WebElement container) {
        //PageFactory.initElements(new DefaultElementLocatorFactory(container),this); nu o vom folosi
        this.container = container;
    }

    public int getNumberOfPRoducts() {
        List<WebElement> products = container.findElements(By.xpath(PRODUCT_XPATH));
        return products.size();
    }

    public ShopHomePageProduct getProductListElement(String productName) {
        WebElement product = container.findElement(By.xpath(PRODUCT_WITH_NAME.replace("<<product-name>>", productName)));
        return new ShopHomePageProduct(product);

    }
}
