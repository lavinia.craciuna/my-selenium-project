package ro.shop;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class ShopHomePageProduct {
    public WebElement container;
    @FindBy(tagName="figure")
    private WebElement image;
    @FindBy(className = "product-list-title")
    private WebElement title;
    @FindBy(className = "price")
    private WebElement price;
    @FindBy(className = "cart-wishlist-btn")
    private WebElement addToCardBtn;

    public ShopHomePageProduct(WebElement product) {
        PageFactory.initElements(new DefaultElementLocatorFactory(product),this);
        this.container = product;
    }

    public String getTitle()
    {
        return title.getText();
    }
    public String getPrice(){
        return price.getText();
    }
    public void addToCard(){
        addToCardBtn.click();
    }
}
